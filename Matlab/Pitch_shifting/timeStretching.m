function D_stretched = timeStretching(D, rate, hop_length)
%PHASE_VOCODER Time-stretching/compression using the phase vocoder
%
%   D_stretched = PHASE_VOCODER(D, rate) stretches or compresses the time 
%   dimension of the complex STFT matrix D by the given rate using the 
%   phase vocoder technique.
%
%   D_stretched = PHASE_VOCODER(D, rate, hop_length) allows specifying the 
%   hop length (number of samples between successive STFT columns). If not 
%   provided, it defaults to n_fft / 4.
%
%   Inputs:
%   - D: Complex-valued STFT matrix (frequency bins x time frames).
%   - rate: Stretching/compression factor (e.g., 1.5 for 50% slower, 0.5 
%           for 50% faster).
%   - hop_length: (Optional) Number of samples between successive STFT 
%                 columns. Default is n_fft / 4.
%
%   Outputs:
%   - D_stretched: Complex-valued STFT matrix with the time dimension 
%                  stretched/compressed by the given rate.
%
%   Example:
%       [x, fs] = audioread('audio.wav');
%       window_duration = 0.025; 
%       ratio_overlap = 0.5; 
%       X = stft(x, fs, window_duration, ratio_overlap);
%       rate = 1.5;
%       X_stretched = phase_vocoder(X, rate);
%       y = istft(X_stretched, fs, window_duration, ratio_overlap);
%
%   Notes:
%   - This function assumes that the input STFT matrix D was generated with 
%     a consistent hop length and window function.
%   - The function performs phase propagation to maintain phase coherence 
%     across frames during stretching/compression.
%
%   See also STFT, ISTFT.

    % Determine the number of FFT samples
    n_fft = 2 * size(D, 1) - 2;

    % Determine the hop length if not provided
    if nargin < 3
        hop_length = floor(n_fft / 4);
    end

    % Calculate the time steps
    time_steps = 1:rate:size(D, 2);

    % Create an empty output array
    D_stretched = zeros(size(D, 1), length(time_steps));

    % Expected phase advance in each bin per frame
    phi_advance = hop_length * (2 * pi * (0:(n_fft / 2))' / n_fft);

    % Phase accumulator; initialize to the first sample
    phase_acc = angle(D(:, 1));

    % Add two extra columns with zeros to ease interpolation on edge
    D = [D, zeros(size(D, 1), 2, 'like', D)];

    for t_idx = 1:length(time_steps)
        step = time_steps(t_idx);

        % Position of the oversampled value in the initial grid
        col_idx = floor(step);
        
        % Extract the 2 consecutive frequency vectors to perform linear interpolation
        columns = D(:, col_idx+1:col_idx+2);

        % Weighting for linear magnitude interpolation
        % alpha coefficient should be between 0 and 1. Between 2 samples
        % alpha = 0.5. 
        alpha = ...
        mag = ...;

        % Store to output array
        D_stretched(:, t_idx) = mag .* exp(1i * phase_acc);

        % Compute phase advance
        dphase = angle(columns(:, 2)) - angle(columns(:, 1)) - phi_advance;

        % Wrap to -pi:pi range
        dphase = dphase - 2 * pi * round(dphase / (2 * pi));

        % Accumulate phase
        phase_acc = phase_acc + phi_advance + dphase;
    end
end
