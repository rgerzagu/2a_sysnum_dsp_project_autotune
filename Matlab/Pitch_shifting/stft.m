function matY = stft(x, fs, window_duration, ratio_overlap, fftSize, window)
%STFT (VERY CUSTOM, MUCH WOW) Short-Time Fourier Transform
%
%   matY = STFT(x, fs, window_duration, ratio_overlap) computes the 
%   Short-Time Fourier Transform (STFT) of the signal x.
%
%   matY = STFT(x, fs, window_duration, ratio_overlap, fftSize) allows 
%   specifying the size of the FFT.
%
%   matY = STFT(x, fs, window_duration, ratio_overlap, fftSize, window) 
%   allows specifying a custom window function.
%
%   Inputs:
%   - x: Input signal (vector).
%   - fs: Sampling frequency of the input signal.
%   - window_duration: Duration of the window in seconds.
%   - ratio_overlap: Ratio of overlap between consecutive windows 
%                    (0 <= ratio_overlap < 1).
%   - fftSize: (Optional) Size of the FFT to be performed. If not provided,
%              it defaults to the window size.
%   - window: (Optional) Handle to a window function (e.g., @hamming, 
%             @hann). Default is @hamming.
%
%   Outputs:
%   - matY: STFT matrix (complex-valued), where each column is the FFT of a 
%           segment of the input signal.
%
%   Example:
%       fs = 16000; 
%       t = 0:1/fs:1-1/fs; 
%       signal = sin(2*pi*440*t); 
%       window_duration = 0.025; 
%       ratio_overlap = 0.5; 
%       matY = stft(signal, fs, window_duration, ratio_overlap);
%
%   Notes:
%   - The function performs an STFT of the input signal x using overlapping 
%     windowed segments.
%   - If the window size is smaller than the FFT size, zero-padding is 
%     performed to match the FFT size.
%
%   See also IFFT, FFT, HAMMING, HANN.


    % Default parameters
    if nargin < 5
        fftSize = 0;
    end
    if nargin < 6
        window = @hamming;
    end
    % Duration in samples of window
    nS = round(window_duration * fs);
    if fftSize == 0
        fftSize = nS;
    end    
    % Assertions
    assert(length(x) > window_duration, 'Signal to analyze is shorter than window duration');
    assert(nS <= fftSize, ...
        ['FFT size (', num2str(fftSize), ') is lower than window duration (', num2str(nS), '). ', ...
         'Try reducing the window duration (', num2str(window_duration), ') to ', ...
         num2str(round(fftSize * window_duration) / fs)]);    
    % Window function
    wind = window(nS);    
    % Position of starting analysis point
    cnt = 0;    
    % Value of shift between consecutive segment analysis
    delta = round(nS * (1 - ratio_overlap));    
    % Number of segments
    nbSeg = floor((length(x) - nS) / delta);    
    % Initialize output matrix
    matY = zeros(fftSize, nbSeg);    
    % Container to speed up
    tmp = zeros(fftSize, 1);    
    for n = 1:nbSeg
        % Get current segment to analyze and apodize it
        tmp(1:nS) = ...      
        % Convert in frequency domain 
        tmp_out = ...    
        % Populate global matrix
        matY(:, n) = tmp_out;        
        % Update counter
        cnt = cnt + delta;
    end
end

