function x = istft(X, fs, window_duration, ratio_overlap, window)
%ISTFT (Very CUSTOM, much WOW) Inverse Short-Time Fourier Transform
%
%   x = ISTFT(X, fs, window_duration, ratio_overlap) reconstructs the 
%   time-domain signal x from its Short-Time Fourier Transform (STFT) 
%   matrix X. 
%
%   x = ISTFT(X, fs, window_duration, ratio_overlap, window) allows 
%   specifying a custom window function.
%
%   Inputs:
%   - X: STFT matrix (complex-valued), where each column is the FFT of a 
%        segment of the original signal.
%   - fs: Sampling frequency of the original signal.
%   - window_duration: Duration of the window in seconds.
%   - ratio_overlap: Ratio of overlap between consecutive windows 
%                    (0 <= ratio_overlap < 1).
%   - window: (Optional) Handle to a window function (e.g., @hamming, 
%             @hann). Default is @hamming.
%
%   Outputs:
%   - x: Reconstructed time-domain signal.
%
%   Example:
%       fs = 16000; 
%       t = 0:1/fs:1-1/fs; 
%       signal = sin(2*pi*440*t); 
%       window_duration = 0.025; 
%       ratio_overlap = 0.5; 
%       X = stft(signal, fs, window_duration, ratio_overlap); 
%       reconstructed_signal = istft(X, fs, window_duration, ratio_overlap);
%
%   Notes:
%   - The function assumes that the input STFT matrix X was generated with 
%     the same window function and overlap ratio.
%   - The function handles zero-padding if the window size is less than 
%     the FFT size.
%
    % Default parameter
    if nargin < 5
        window = @hamming;
    end    
    fftSize = size(X, 1);  % FFT size done in tfGrid
    nbSeg = size(X, 2);    % Number of segments    
    % Know if zero padding has been performed in direct transform
    nS = round(window_duration * fs);  % Duration of each segment of the STFT
    win = window(nS);    
    % Getting final size of x assuming overlap
    delta = round(nS * (1 - ratio_overlap));
    size_x = delta * (nbSeg - 1) + nS;
    x = zeros(size_x, 1);    
    % Know if zero padding has been used for analysis
    zeroPadding = (nS < fftSize);
    cnt = 0;    
    % Determine k based on zero padding
    if zeroPadding
        k = ceil(fftSize / nS);
    else
        k = 1;
    end    
    % Determine the size of the FFT in Rx that will be a multiple of segment duration
    rxFFT = k * nS;    
    % --- Processing column by column
    for n = 1:nbSeg
        % Container for Rx
        tmp = zeros(rxFFT,1);        
        % Filling signal, end is 0 --> ZP
        tmp(1:fftSize) = ...        
        % Go back in time domain 
        out = ...
        % Only part of this signal is usefull, and we need to apodise it 
        out2 = ...
        % Signal is now in time domain and oversampled at rate k
        % Overlap and Add (OLA) approach
        x(cnt + (1:nS)) = x(cnt + (1:nS)) + real(out2);
        % Update the counter 
        cnt = cnt + delta;
    end    
    % Scale the output
    x = x * sqrt(fftSize / nS / 2);
end
