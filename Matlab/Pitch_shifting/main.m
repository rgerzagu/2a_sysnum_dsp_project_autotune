% --- This code is for the last iteration of the project 
% Purpose --> Apply pitch shifting to a WAV file.
% Feel free to use it with your custom sounds 
% 2 signals are important in the end 
%   - sig    : Intial audio signal 
%   - sigOut : Shifted audio signal 
% You can use sound to hear it

% --- Loading the audio file 
[sig,fs] = audioread('../../Base_Sound/libre_de_droits.wav');

% --- Desired Pitch frequency parameters 
% Physical explanation is on the project topic 
n_steps  = -8;         % How many semitones we will shift 
bins_per_octave = 12;  % Western music octave definition 
rate = 2.0 ^ (- (n_steps) / bins_per_octave);


% --- Parameter for the frequency analysis 
window_duration = 0.2;   % In seconds 
ratio_overlap = 0.5;     % 50% overlap 

% --- Pitch Shifting algorithm 
% First, switch in T/F domain with overlap 
% /!\ This function has to be completed (go in the file)
A = stft(sig, fs, window_duration, ratio_overlap);
% Then we can apply the time stretching with linear interpolation 
% /!\ This function has to be completed (go in the file)
Z = timeStretching(A,rate);
% Then, the signal is re-compressed to the initial time base, leading to
% shift in frequency 
% We need to go back in time 
% /!\ This function has to be completed (go in the file)
ZZ = istft(Z, fs, window_duration, ratio_overlap);
% And resample 
[p,q] = rat(rate);
sigOut = resample(ZZ,p,q);

